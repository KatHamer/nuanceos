; NuanceOS - IO
; IO Functions for NuanceOS
; By Kat Hamer

print:  ; Write a string to the screen
    mov ax, 0       ; Buffer to set extra segment register
    mov es, ax      ; Set extra segment register to 0
    mov bh, 0       ; Video page number
    mov al, 1       ; Subservice
    mov ah, 0x13    ; BIOS call for print string
    int 0x10        ; Call BIOS

    mov ax, [row]  ; Move row counter into memory
    inc ax         ; Increment row counter
    mov [row], ax  ; Move row counter from memory


    ret             ; Return

read: ; Read a key and store the result in AL
    mov ah, 0             ; BIOS call for wait for keypress
    int 16h               ; Call bios
    ret

clear:
    mov dx, 0  ; Set cursor to top left
    call set_cursor
    mov ah, 06h  ; BIOS call for scroll screen
    mov al, 0    ; Lines to blank at bottom
    mov bh, 7    ; Attribute to use on blank line
    mov cx, 0    ; Top left
    mov dh, 24   ; Bottom right
    mov dl, 79
    int 10h

    mov ax, 0
    mov [row], ax

    ret

set_cursor:
    mov bh, 0  ; Video page number
    mov ah, 2  ; BIOS call for move cursor
    int 10h    ; Call BIOS
    ret

wait_for_key: ; Wait for a key press
       mov bp, waitforpress      ; String to display
       mov cx, waitforpress_len  ; Length of string
       mov dl, 0                 ; X position of string
       mov dh, [row]             ; Y position of string
       call print                ; Display prompt
       call read                 ; Read a key
       ret

row dw 0
waitforpress db "Press any key..."
waitforpress_len equ $-waitforpress
