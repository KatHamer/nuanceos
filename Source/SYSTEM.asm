; NuanceOS - System
; Miscellaneous system data
; By Kat Hamer


welcome: ; Display a welcome message
    call clear
    mov bl, 0x2
    mov bp, welcome1
    mov cx, welcome1_len
    mov dl, 0
    mov dh, [row]
    call print
    mov bp, welcome2
    mov cx, welcome2_len
    mov dl, 0
    mov dh, [row]
    call print
    mov bp, newline
    mov cx, 0
    mov dl, 0
    mov dh, [row]
    call print
    mov si, 1
    call wait_for_key
    ret

reboot:
    call clear
    mov bp, rebooting
    mov cx, rebooting_len
    mov dl, 0
    mov dh, 0
    call print
    call wait_for_key
    mov ax, 0
    int 19h   ; Reboot system

rebooting db "Rebooting system..."
rebooting_len equ $-rebooting
welcome1 db "Welcome to NaunceOS!"
welcome1_len equ $-welcome1
welcome2 db "Programmed with ",03h," by Kat Hamer"
welcome2_len equ $-welcome2
newline db ""
