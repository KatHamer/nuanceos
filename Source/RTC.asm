; NuanceOS - RTC
; Time and RTC related functions
; By Kat Hamer

get_rtc:
    mov ah, 0  ; BIOS call for get ticks since midnight
    int 01ah   ; Call BIOS RTC functions
    ;cx:dx
