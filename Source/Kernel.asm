; NuanceOS - Kernel
; Micro Kernel for NuanceOS
; By Kat Hamer

bits 16  ; We need to work in 16 bits
org 0x7c00  ; Start execution at this offset in memory


start: ; Initial boot code
    call welcome
    call test_colors

%include "IO.asm"       ; Include IO module
%include "SYSTEM.asm"   ; Include SYSTEM module
%include "TEST.asm"      ; Include TEST module

times 510-($-$$) db 0  ; Pad remainder of boot sector with 0s
dw 0xAA55  ; Boot signature to tell BIOS that we are bootable
