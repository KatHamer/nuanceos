; NuanceOS - TEST
; Testing and debugging
; By Kat Hamer

test_colors:
    mov bp, testing
    mov cx, testing_len
    mov dl, 0
    mov dh, [row]
    call print

    mov al, row ; Preserve row variable
    mov bl, 15  ; Execute 15 times for 15 colours
    jmp loop_colors
    loop_colors:
        dec bl
        mov bp, char  ; Rect
        mov cx, 1
        mov dl, bl
        mov dh, 6
        call print
        cmp bl, 0
        jne loop_colors
        ret

char db 0xDB
char_len equ $-char
testing db "Testing colours:"
testing_len equ $-testing
